import matplotlib.pyplot as plt
from savi.vigas import VigaBiapoiada, VigaEngastada
from savi.cargas import Pontual, LinearmenteDistribuida

def teste1():
     # Link da Viga: viga.online/#L(10):P(1)R(9):F(3,5)F(9,10)W(2,8,3,3)
    viga = VigaBiapoiada(10, 1, 9)

    cargas = [
        Pontual(posicao=3, modulo=5),
        Pontual(posicao=9, modulo=10),
        LinearmenteDistribuida(posicao1=2, posicao2=8, modulo=3)
    ]
    
    viga.aplica_cargas(cargas)
    Rv1, Rv2 = viga.reacoes_apoio()
    print('Rv1 = {} e  Rv2 = {}'.format(Rv1, Rv2))

    Mx = viga.momento_fletor()
    plt.plot(viga.x, Mx)
    plt.title("Momento fletor")
    plt.show(block=False)

    Ec = viga.esforco_cortante()
    plt.figure()
    plt.plot(viga.x, Ec)
    plt.title("Esforço cortante")
    plt.show()

def teste2():
    # Link da Viga: viga.online/#L(10):E(0):F(3,5)F(9,10)W(2,8,3,3)
    viga = VigaEngastada(10, 0)

    cargas = [
        Pontual(posicao=3, modulo=5),
        Pontual(posicao=9, modulo=10),
        LinearmenteDistribuida(posicao1=2, posicao2=8, modulo=3)
    ]
    
    viga.aplica_cargas(cargas)
    Rv, Rm = viga.reacoes_apoio()
    print('Rv = {} e  Rm = {}'.format(Rv, Rm))

    Mx = viga.momento_fletor()
    plt.plot(viga.x, Mx)
    plt.title("Momento fletor")
    plt.show(block=False)

    Ec = viga.esforco_cortante()
    plt.figure()
    plt.plot(viga.x, Ec)
    plt.title("Esforço cortante")
    plt.show()

if __name__ == '__main__':
   #teste1()
   teste2()