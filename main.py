import sys
from PyQt5.QtWidgets import QApplication
from savi.ui.principal import Principal

if __name__ == "__main__":
    app = QApplication(sys.argv)
    p = Principal()
    p.show()
    sys.exit(app.exec_())

