# SAVI

Cálculo de forças de reação nos apoios de vigas isostáticas.

## PIBIC-EM

Trabalho desenvolvido como projeto de PIBIC-EM no IFMS Aquidauana.
IFMS Aquidauana - PIBIC-EM

### Estudantes:
* Allan dos Santos Arguelho
* Ronaldo Ferreira Borges Júnior

### Orientadores:
* Luan Matheus Moreira
* Lucas Hermann Negri

## Resumo

Na unidade curricular de Resistência dos Materiais estuda-se a análise de vigas isostáticas, com o intuito de calcular as reações de apoio e os esforços internos (e. g., esforços normais, esforços cortantes, momentos fletores). Na grade curricular do curso, não há nenhuma unidade curricular relacionada ao desenvolvimento de programas de computador, logo, o ensino fica limitado ao desenvolvimento manual dos cálculos matemáticos propostos. Contudo, os softwares educacionais disponíveis necessitam que o usuário tenha conhecimento prévio dos conceitos científicos que os fundamentam. Com o intuito de preencher esta lacuna, este projeto propõe o desenvolvimento de um software educacional para análise de vigas isostáticas, orientado ao ensino-aprendizagem. Desta forma, espera-se que este programa computacional seja utilizado como ferramenta auxiliar pelos docentes desta unidade curricular no IFMS e demais instituições de ensino existentes em âmbito nacional.

## Licença

GPLv3+.