from savi.cargas.util import calc_momento

class LinearmenteDistribuida:
    """Força linearmente distribuída entre dois pontos"""

    def __init__(self, posicao1, posicao2, modulo):
        self.posicao1 = posicao1
        self.posicao2 = posicao2
        self.modulo = modulo

    def z(self):
        return self.posicao2 - self.posicao1

    def momento(self, apoio1):
        return calc_momento(self.modulo * self.z(), (self.posicao1 + self.z() / 2) - apoio1, 0)

    def momento_fletor(self, x):
        Mx = (self.modulo / 2) * (x - self.posicao1)**2
        Mx[x < self.posicao1] = 0
        qR = self.z() * self.modulo
        Mx[x > self.posicao2] = qR * (x[x > self.posicao2] - (self.posicao1 + self.z() / 2))
        return -Mx

    def esforco_cortante(self, x):
        Fx = self.modulo * (x - self.posicao1)
        Fx[x < self.posicao1] = 0
        Fx[x > self.posicao2] = self.modulo * self.z()
        return -Fx