import numpy as np
from savi.cargas.util import calc_momento

class Pontual:
    """Força pontual"""

    def __init__(self, posicao, modulo):
        """
        posicao: Posição da carga [m]
        modulo: Módulo da carga [kN]
        """
        self.posicao = posicao
        self.modulo = modulo

    def momento(self, apoio1):
        """
        apoio1: Posição do apoio 1 [m]
        """
        return calc_momento(self.modulo, self.posicao, apoio1)

    def momento_fletor(self, x):
        """
        x: Vetor representando os pontos discretizados da viga [m]
        """
        Mx = self.modulo * (x - self.posicao)
        Mx[x < self.posicao] = 0
        return -Mx

    def esforco_cortante(self, x):
        Fx = np.ones(x.shape) * self.modulo
        Fx[x < self.posicao] = 0
        return -Fx