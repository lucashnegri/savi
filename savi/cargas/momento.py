import numpy as np

class Momento:
    """Momento"""
    def __init__(self, posicao, modulo):
        self.posicao = posicao
        self.modulo = modulo
    
    def momento(self, apoio1):
        return self.modulo, 0

    def momento_fletor(self, x):
        Mx = np.ones(x.shape) * self.modulo
        Mx[x < self.posicao] = 0
        return -Mx

    def esforco_cortante(self, x):
        return np.zeros(x.shape)