import numpy as np
from savi.vigas import Viga

class VigaEngastada(Viga):
    def __init__(self, comprimento, apoio1):
        """Inicializa a viga engastada"""
        super().__init__(comprimento, apoio1)

    def reacoes_apoio(self):
        """Calcula as reações vertical e de momento no único apoio"""
        Rv = sum(self.forcas)      # reação vertical do apoio
        Rm = sum(self.momentos)    # reação de momento do apoio
        return Rv, Rm

    def momento_fletor(self):
        Mf = self.Mf.copy()

        Rv, Rm = self.reacoes_apoio()
        
        Mx = Rv * (self.x - self.apoio1)
        Mx[self.x < self.apoio1] = 0
        Mf += Mx

        Mx = np.ones(self.x.shape) * Rm
        Mx[self.x < self.apoio1] = 0
        Mf += Mx

        return -Mf

    def esforco_cortante(self):
        Rv, _ = self.reacoes_apoio()
        Fx = np.ones(self.x.shape) * Rv
        Fx[self.x < self.apoio1] = 0
        return self.Ec + Fx