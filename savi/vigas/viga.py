import numpy as np

class Viga:
    def __init__(self, comprimento, apoio1, resolucao=1000):
        self.comprimento = comprimento
        self.apoio1 = apoio1
        self.x = np.linspace(0, self.comprimento, num=resolucao)
        
        # outros atributos, inicializados pelo aplica_cargas
        self.cargas = None
        self.momentos = None
        self.forcas = None
        self.Mf = None
        self.Ec = None

    def aplica_cargas(self, cargas):
        self.cargas = cargas
        self.momentos, self.forcas = zip(*(c.momento(self.apoio1) for c in cargas))
        self.Mf = sum(c.momento_fletor(self.x) for c in self.cargas)
        self.Ec = sum(c.esforco_cortante(self.x) for c in self.cargas)