import numpy as np
from savi.vigas import Viga

class VigaBiapoiada(Viga):
    def __init__(self, comprimento, apoio1, apoio2):
        """Inicializa a viga biapoiada"""
        super().__init__(comprimento, apoio1)
        self.apoio2 = apoio2

    def reacoes_apoio(self):
        Rv2 = sum(self.momentos) / (self.apoio2 - self.apoio1)   # reação vertical no apoio 2
        Rv1 = sum(self.forcas) - Rv2                             # reação vertical no apoio 1
        return Rv1, Rv2

    def momento_fletor(self):
        Mf = self.Mf.copy()

        Rv1, Rv2 = self.reacoes_apoio()

        for apoio, Rv in [(self.apoio1, Rv1), (self.apoio2, Rv2)]:
            Mx = Rv * (self.x - apoio)
            Mx[self.x < apoio] = 0
            Mf += Mx

        return -Mf

    def esforco_cortante(self):
        Ec = self.Ec.copy()
        
        Rv1, Rv2 = self.reacoes_apoio()

        for apoio, Rv in [(self.apoio1, Rv1), (self.apoio2, Rv2)]:
            Fx = np.ones(self.x.shape) * Rv
            Fx[self.x < apoio] = 0
            Ec += Fx

        return Ec