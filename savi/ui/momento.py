from PyQt5.QtWidgets import QDialog
from savi.ui.gen.dialogoMomento import Ui_Momento

class DialogoMomento(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Momento()
        self.ui.setupUi(self)

    def rodar(self):
        self.ui.posicao.setFocus()
        res = self.exec_()

        if res == QDialog.Accepted:
            apoio = self.ui.posicao.value()
            momento = self.ui.forca.value()
            return "Momento", apoio, "", momento