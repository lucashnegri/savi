from PyQt5.QtWidgets import QDialog
from savi.ui.gen.dialogoPontual import Ui_Pontual

class DialogoPontual(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Pontual()
        self.ui.setupUi(self)

    def rodar(self):
        self.ui.posicaoApoio.setFocus()
        res = self.exec_()

        if res == QDialog.Accepted:
            apoio = self.ui.posicaoApoio.value()
            momento = self.ui.forcaMomento.value()
            return "Pontual", apoio, "", momento