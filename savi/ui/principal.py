from enum import IntEnum

# Qt
from PyQt5.QtWidgets import QWidget, QTableWidgetItem, QMessageBox
from PyQt5.QtCore import Qt

# matplotlib
import matplotlib.pyplot as plt

# UI
from savi.ui.gen.principal import Ui_Principal

from savi.ui.pontual import DialogoPontual
from savi.ui.linear import DialogoLinear
from savi.ui.momento import DialogoMomento

# Cálculos
from savi.vigas import VigaBiapoiada, VigaEngastada
from savi.cargas import Pontual, Momento, LinearmenteDistribuida

class Forca(IntEnum):
    PONTUAL = 0
    LINEAR = 1
    MOMENTO = 2

class Principal(QWidget):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Principal()
        self.ui.setupUi(self)
        self.dialogoPontual = DialogoPontual()
        self.dialogoLinear = DialogoLinear()
        self.dialogoMomento = DialogoMomento()

    def biapoiadoClicked(self):
        self.ui.posicao1.setEnabled(True)
        self.ui.posicao2.setEnabled(True)

    def engastadoClicked(self):
        self.ui.posicao1.setEnabled(True)
        self.ui.posicao2.setEnabled(False)

    def adicionaForca(self, tipo):
        if tipo == Forca.PONTUAL:
            res = self.dialogoPontual.rodar()
        elif tipo == Forca.LINEAR:
            res = self.dialogoLinear.rodar()
        elif tipo == Forca.MOMENTO:
            res = self.dialogoMomento.rodar()
        else:
            raise ValueError("Tipo de força inválido")

        if not res:
            return

        self.ui.telaCarga.insertRow(self.ui.telaCarga.rowCount())

        for col in range(4):
            item = QTableWidgetItem(str(res[col]))
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)

            self.ui.telaCarga.setItem(
                self.ui.telaCarga.rowCount() - 1, col, item)

    def remover(self):
        row = self.ui.telaCarga.currentRow()

        if row >= 0:
            self.ui.telaCarga.removeRow(row)

    def calcular(self):
        comprimento = self.ui.comprimentoViga.value()
        posicao1 = self.ui.posicao1.value()
        engastada = self.ui.engastado.isChecked()
        biapoiada = self.ui.biapoiado.isChecked()

        # cria a viga de acordo im os dados fornecidos
        if engastada:
            viga = VigaEngastada(comprimento, posicao1)
        elif biapoiada:
            posicao2 = self.ui.posicao2.value()
            viga = VigaBiapoiada(comprimento, posicao1, posicao2)
        else:
            raise ValueError("Tipo inválido de viga")

        # configura as cargas de acordo com a tabela de cargas
        cargas = []

        # adicionar as cargas descritas na tabela
        modelo = self.ui.telaCarga.model()

        for row in range(modelo.rowCount()):
            tipo = modelo.data(modelo.index(row, 0))
            pos1 = float(modelo.data(modelo.index(row, 1)))
            pos2 = modelo.data(modelo.index(row, 2)) # nem sempre é usado
            modulo = float(modelo.data(modelo.index(row, 3)))

            if tipo == 'Pontual':
                cargas.append(Pontual(pos1, modulo))
            elif tipo == 'Linear':
                cargas.append(LinearmenteDistribuida(pos1, float(pos2), modulo))
            elif tipo == 'Momento':
                cargas.append(Pontual(pos1, modulo))
            else:
                raise ValueError("Tipo de carga inválido")

        try:
            # aplica as cargas e obtém as reações nos apoios
            viga.aplica_cargas(cargas)
            r1, r2 = viga.reacoes_apoio()

            if engastada:
                msg = """
Rv = {:.2f} kN e  Rm = {:.2f} kN'

Rv = É a reação vertical do apoio localizado na posição 1.
Rm = É a reação de giro do apoio localizado na posição 1.
""".format(r1, r2)
            else:
                # biapoiada
                msg = """
Rv1 = {:.2f} kN e  Rv2 = {:.2f} kN

Rv1 = É a reação vertical do apoio localizado na posição 1. 
Rv2 = É a reação vertical do apoio localizado na posição 2.
""".format(r1, r2)

            QMessageBox.information(self, 'Reação', msg)

            # mostra os gráficos do momento fletor e esforço cortante
            Mx = viga.momento_fletor()
            plt.plot(viga.x, Mx)
            plt.title("Diagrama de Momento Fletor")
            plt.xlabel("Comprimento da viga (m)")
            plt.ylabel("Momento fletor (em kN.m)")
            plt.show(block=False)

            Ec = viga.esforco_cortante()
            plt.figure()
            plt.plot(viga.x, Ec)
            plt.title("Diagrama de Esforço cortante")
            plt.xlabel("Comprimento da viga (m)")
            plt.ylabel("Esforço cortante (em kN)")
            plt.show()
        except ZeroDivisionError:
            QMessageBox.critical(self, 'Erro', 'Divisão por zero.\nReveja os parâmetros fornecidos.')
        except ValueError:
            QMessageBox.critical(self, 'Erro', 'Nenhuma carga fornecida.\nAdicione ao menos uma carga.')
        except Exception:
            QMessageBox.critical(self, 'Erro', 'Valores inválidos.\nReveja os parâmetros fornecidos.')
