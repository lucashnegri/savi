# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/dialogoLinear.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Linear(object):
    def setupUi(self, Linear):
        Linear.setObjectName("Linear")
        Linear.resize(238, 146)
        self.verticalLayout = QtWidgets.QVBoxLayout(Linear)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setSpacing(0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(Linear)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.posicaoInicio = QtWidgets.QDoubleSpinBox(Linear)
        self.posicaoInicio.setMaximum(99.0)
        self.posicaoInicio.setObjectName("posicaoInicio")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.posicaoInicio)
        self.label_2 = QtWidgets.QLabel(Linear)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.posicaoFinal = QtWidgets.QDoubleSpinBox(Linear)
        self.posicaoFinal.setMinimum(1.0)
        self.posicaoFinal.setMaximum(100.0)
        self.posicaoFinal.setObjectName("posicaoFinal")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.posicaoFinal)
        self.label_3 = QtWidgets.QLabel(Linear)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.forca = QtWidgets.QDoubleSpinBox(Linear)
        self.forca.setMaximum(100.0)
        self.forca.setObjectName("forca")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.forca)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(Linear)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Linear)
        self.buttonBox.accepted.connect(Linear.accept)
        self.buttonBox.rejected.connect(Linear.reject)
        QtCore.QMetaObject.connectSlotsByName(Linear)

    def retranslateUi(self, Linear):
        _translate = QtCore.QCoreApplication.translate
        Linear.setWindowTitle(_translate("Linear", "Linearmente Distribuída"))
        self.label.setText(_translate("Linear", "Posição inicial:"))
        self.posicaoInicio.setSuffix(_translate("Linear", " m"))
        self.label_2.setText(_translate("Linear", "Posição final:"))
        self.posicaoFinal.setSuffix(_translate("Linear", " m"))
        self.label_3.setText(_translate("Linear", "Módulo:"))
        self.forca.setSuffix(_translate("Linear", " kN/m"))

