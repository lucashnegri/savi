# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/dialogoPontual.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Pontual(object):
    def setupUi(self, Pontual):
        Pontual.setObjectName("Pontual")
        Pontual.resize(186, 115)
        self.verticalLayout = QtWidgets.QVBoxLayout(Pontual)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setSpacing(0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(Pontual)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.posicaoApoio = QtWidgets.QDoubleSpinBox(Pontual)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.posicaoApoio.sizePolicy().hasHeightForWidth())
        self.posicaoApoio.setSizePolicy(sizePolicy)
        self.posicaoApoio.setMaximum(100.0)
        self.posicaoApoio.setObjectName("posicaoApoio")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.posicaoApoio)
        self.label_2 = QtWidgets.QLabel(Pontual)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.forcaMomento = QtWidgets.QDoubleSpinBox(Pontual)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.forcaMomento.sizePolicy().hasHeightForWidth())
        self.forcaMomento.setSizePolicy(sizePolicy)
        self.forcaMomento.setMaximum(100.0)
        self.forcaMomento.setObjectName("forcaMomento")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.forcaMomento)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(Pontual)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Pontual)
        self.buttonBox.accepted.connect(Pontual.accept)
        self.buttonBox.rejected.connect(Pontual.reject)
        QtCore.QMetaObject.connectSlotsByName(Pontual)

    def retranslateUi(self, Pontual):
        _translate = QtCore.QCoreApplication.translate
        Pontual.setWindowTitle(_translate("Pontual", "Pontual"))
        self.label.setText(_translate("Pontual", "Posição:"))
        self.posicaoApoio.setSuffix(_translate("Pontual", " m "))
        self.label_2.setText(_translate("Pontual", "Módulo:"))
        self.forcaMomento.setSuffix(_translate("Pontual", " kN"))

