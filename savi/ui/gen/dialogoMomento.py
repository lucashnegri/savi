# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/dialogoMomento.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Momento(object):
    def setupUi(self, Momento):
        Momento.setObjectName("Momento")
        Momento.resize(190, 115)
        self.verticalLayout = QtWidgets.QVBoxLayout(Momento)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setSpacing(0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(Momento)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.posicao = QtWidgets.QDoubleSpinBox(Momento)
        self.posicao.setMaximum(100.0)
        self.posicao.setObjectName("posicao")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.posicao)
        self.label_2 = QtWidgets.QLabel(Momento)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.forca = QtWidgets.QDoubleSpinBox(Momento)
        self.forca.setMaximum(100.0)
        self.forca.setObjectName("forca")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.forca)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(Momento)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Momento)
        self.buttonBox.accepted.connect(Momento.accept)
        self.buttonBox.rejected.connect(Momento.reject)
        QtCore.QMetaObject.connectSlotsByName(Momento)

    def retranslateUi(self, Momento):
        _translate = QtCore.QCoreApplication.translate
        Momento.setWindowTitle(_translate("Momento", "Momento"))
        self.label.setText(_translate("Momento", "Posição:"))
        self.posicao.setSuffix(_translate("Momento", " m "))
        self.label_2.setText(_translate("Momento", "Módulo:"))
        self.forca.setSuffix(_translate("Momento", " kN.m"))

