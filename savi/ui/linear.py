from PyQt5.QtWidgets import QDialog
from savi.ui.gen.dialogoLinear import Ui_Linear

class DialogoLinear(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Linear()
        self.ui.setupUi(self)

    def rodar(self):
        self.ui.posicaoInicio.setFocus()
        res = self.exec_()

        if res == QDialog.Accepted:
            inicial = self.ui.posicaoInicio.value()
            final = self.ui.posicaoFinal.value()
            momento = self.ui.forca.value()
            return "Linear", inicial, final, momento